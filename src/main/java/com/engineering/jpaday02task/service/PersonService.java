package com.engineering.jpaday02task.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.engineering.jpaday02task.entity.City;
import com.engineering.jpaday02task.entity.Contact;
import com.engineering.jpaday02task.entity.Person;

public class PersonService {
	private EntityManagerFactory emf;

	public PersonService(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public Person findById(Long id) throws Exception {
		EntityManager em = emf.createEntityManager();

		List<Person> persons = em
				.createQuery("SELECT DISTINCT p FROM Person p" 
						+ " LEFT JOIN FETCH p.hobbies h"
						+ " LEFT JOIN FETCH p.contacts c WHERE p.id = :id")
				.setParameter("id", id).getResultList();
		try {
			if (persons.size() == 0)
				throw new Exception("Ne postoji osoba");		

			return persons.get(0);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}

	}

	public Person saveOrUpdate(Person person) throws Exception {
		EntityManager em = emf.createEntityManager();

		if (person.getId() != null) {
			Person existingPerson = em.find(Person.class, person.getId());
			if (existingPerson == null)
				throw new Exception("Osoba ne postoji!");
		}

		try {
			em.getTransaction().begin();
			// da li postoji grad
			if (person.getBornCity() == null || person.getBornCity().getId() == null) {
				throw new Exception("Grad nema ID...");
			}
			City city = em.find(City.class, person.getBornCity().getId());

			if (city == null)
				throw new Exception("Grad sa ovim ID-jem ne postoji...");

			// TODO validacija na vrednosti za osobu

			// TODO da li osoba sa JMBG-om vec postoji u tabeli

			person.setBornCity(city);
			person = em.merge(person);

			em.getTransaction().commit();

			System.out.println("ID: " + person.getId());
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}

		return person;
	}

	public void delete(Person person) throws Exception {
		EntityManager em = emf.createEntityManager();

		Person tempPerson = em.find(Person.class, person.getId());
		try {
			if (tempPerson == null)
				throw new Exception("Osoba ne postoji");

			em.getTransaction().begin();

			tempPerson.getContacts();

			em.remove(tempPerson);

			em.getTransaction().commit();
			System.out.println("Osoba obrisana");
		} catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
			throw e;
		} finally {
			em.close();
		}
	}

	public List<Person> findAll() {
		return null;
	}

	public List<Person> findByCity(City city) {
		EntityManager em = emf.createEntityManager();

		List<Person> persons = em
				.createQuery("SELECT DISTINCT p FROM Person p" + " LEFT JOIN FETCH p.hobbies h"
						+ " LEFT JOIN FETCH p.contacts c WHERE p.bornCity = :city")
				.setParameter("city", city).getResultList();

		return persons;
	}
}
