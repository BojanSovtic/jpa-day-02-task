package com.engineering.jpaday02task;

import java.util.List;

import javax.persistence.Persistence;

import com.engineering.jpaday02task.entity.City;
import com.engineering.jpaday02task.entity.Contact;
import com.engineering.jpaday02task.entity.ContactType;
import com.engineering.jpaday02task.entity.Hobby;
import com.engineering.jpaday02task.entity.Person;
import com.engineering.jpaday02task.service.CityService;
import com.engineering.jpaday02task.service.PersonService;

public class Start {
	private final CityService cityService;
	private final PersonService personService;
	
	public Start() {
		cityService = new CityService(Persistence.createEntityManagerFactory("JPADay02"));
		personService = new PersonService(Persistence.createEntityManagerFactory("JPADay02"));
	}
	
	public static void main(String[] args) {
		Start start = new Start();
		// start.citySaveOrUpdate();
		
		start.personFindByCity();
		
		// start.personSaveOrUpdate();
		
		// start.deletePerson(new Person(5L, "Nebitno", 
		// 		"Nebitno", "Nebitno", null, null));  // moze i samo id
		
		System.out.println("\n\nPronadji osobu po id: ");
		start.findPerson();
	}

	private void findPerson() {
		try {
			Person person = personService.findById(8L);
			
			System.out.println(person);
			
			printContacts(person);
			
			printHobbies(person);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void printContacts(Person person) {
		for (Contact contact : person.getContacts()) {
			System.out.println("\t" + contact);
		}
	}

	private void printHobbies(Person person) {
		for (Hobby hobby : person.getHobbies()) {
			System.out.println("\t" + hobby);
		}
	}

	private void citySaveOrUpdate() {
		City city = new City();
		city.setId(2L);
		city.setNumber(11070L);
		city.setName("Novi Beograd");
		
		try {
			cityService.saveOrUpdate(city);
			System.out.println(city);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

	private void personSaveOrUpdate() {
		City city = new City();
		city.setId(2L);
		
		Person person = new Person();
		person.setId(null);
		person.setFirstname("Marko");
		person.setLastname("Markovic");
		person.setPersonalIdentityNumber("1234567891234");
		
		person.setBornCity(city);
		
		person.addContact(new Contact(null, "marko@gmail.com", ContactType.EMAIL));
		person.addContact(new Contact(null, "011 9999 999", ContactType.FIKSNI_TELEFON));
		
		person.addHobby(new Hobby(3L, "Sah"));
		person.addHobby(new Hobby(2L, "Gitara"));
		
		try {
			person = personService.saveOrUpdate(person);
			System.out.println("Sacuvana je osoba sa kontaktima " + person);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

	private void personFindByCity() {
		City city = new City();
		city.setId(2L);
		List<Person> persons = personService.findByCity(city);
		for (Person person : persons) {
			System.out.println(person);
			printContacts(person);
			
			printHobbies(person);
		}
	}
	
	private void deletePerson(Person person) {
		try {
			personService.delete(person);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
