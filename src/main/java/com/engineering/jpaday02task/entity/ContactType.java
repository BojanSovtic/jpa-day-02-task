package com.engineering.jpaday02task.entity;

public enum ContactType {
	EMAIL, FIKSNI_TELEFON, MOBILNI_TELEFON, WEB_STRANICA, LINKEDIN, GITLAB
}
