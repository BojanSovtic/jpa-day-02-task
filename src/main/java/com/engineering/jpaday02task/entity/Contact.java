package com.engineering.jpaday02task.entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Contact implements Serializable {
	private static final long serialVersionUID = 98022021090700L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String value;

	@Enumerated(EnumType.STRING)
	private ContactType tip;

	@ManyToOne
	private Person person;

	public Contact() {
	}

	public Contact(Long id, String value, ContactType tip) {
		this.id = id;
		this.value = value;
		this.tip = tip;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ContactType getTip() {
		return tip;
	}

	public void setTip(ContactType tip) {
		this.tip = tip;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", value=" + value + ", tip=" + tip + ", personId=" + person.getId() + "]";
	}

}
