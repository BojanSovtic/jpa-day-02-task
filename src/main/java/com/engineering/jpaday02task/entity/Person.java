package com.engineering.jpaday02task.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Person implements Serializable {
	private static final long serialVersionUID = 19022021135800L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "personal_identity_number")
	private String personalIdentityNumber;
	private String firstname;
	private String lastname;

	@ManyToOne
	@JoinColumn(name = "born_city_id")
	private City bornCity;

	@OneToMany(mappedBy = "person", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private Set<Contact> contacts;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "person_hobby", joinColumns = { @JoinColumn(name = "person_id") }, 
	inverseJoinColumns = { @JoinColumn(name = "hobby_id") })
	private Set<Hobby> hobbies;

	public Person() {
		contacts = new HashSet<>();
		hobbies = new HashSet<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPersonalIdentityNumber() {
		return personalIdentityNumber;
	}

	public void setPersonalIdentityNumber(String personalIdentityNumber) {
		this.personalIdentityNumber = personalIdentityNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public City getBornCity() {
		return bornCity;
	}

	public void setBornCity(City bornCity) {
		this.bornCity = bornCity;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public Set<Hobby> getHobbies() {
		return hobbies;
	}

	public void setHobbies(Set<Hobby> hobbies) {
		this.hobbies = hobbies;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bornCity == null) ? 0 : bornCity.hashCode());
		result = prime * result + ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((personalIdentityNumber == null) ? 0 : personalIdentityNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (bornCity == null) {
			if (other.bornCity != null)
				return false;
		} else if (!bornCity.equals(other.bornCity))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (personalIdentityNumber == null) {
			if (other.personalIdentityNumber != null)
				return false;
		} else if (!personalIdentityNumber.equals(other.personalIdentityNumber))
			return false;
		return true;
	}

	public void addContact(Contact contact) {
		contact.setPerson(this);
		this.contacts.add(contact);
	}
	
	public void addHobby(Hobby hobby) {
		this.hobbies.add(hobby);
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", personalIdentityNumber=" + personalIdentityNumber + ", firstname=" + firstname
				+ ", lastname=" + lastname + ", bornCity=" + bornCity + ", contacts=" + contacts + ", hobbies="
				+ hobbies + "]";
	}

}
